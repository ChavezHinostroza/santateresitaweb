﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ColegioST.Models
{
    public class Curso
    {
        public int CursoId { get; set; }

        [Required(ErrorMessage = "Debes ingresar un {0}")]
        [StringLength(30, ErrorMessage =
            "El campo {0} debe contener maximo {1} y minimo {2} caracteres",
            MinimumLength = 2)]
        [Display(Name = "Nombre")]
        [RegularExpression(@"^([a-zA-Z \.\&\'\-]+)$", ErrorMessage = "Nombre invalido")]
        public string Nombre { get; set; }
        public string Grado { get; set; }
        public string Seccion { get; set; }
        public bool Estado { get; set; }

        //fk dcocente
        public int DocenteId { get; set; }
        public Docente Docente { get; set; }

        //muchos a muchos
        public virtual ICollection<AlumnoCurso> AlumnoCurso { get; set; }
    }
}