﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ColegioST.Models
{
    public class AlumnoCurso
    {
        [Key, Column(Order = 0)]
        public int AlumnoId { get; set; }
        [Key, Column(Order = 1)]
        public int CursoId { get; set; }

        public virtual Curso Curso { get; set; }
        public virtual Alumno Alumno { get; set; }

        [RegularExpression(@"^[0-9]+([.][0-9]+)?$", ErrorMessage = "Número decimal no válido")]
        public decimal Examen1 { get; set; }

        [RegularExpression(@"^[0-9]+([.][0-9]+)?$", ErrorMessage = "Número decimal no válido")]
        public decimal Examen2 { get; set; }

        [RegularExpression(@"^[0-9]+([.][0-9]+)?$", ErrorMessage = "Número decimal no válido")]
        public decimal Ti1 { get; set; }

        [RegularExpression(@"^[0-9]+([.][0-9]+)?$", ErrorMessage = "Número decimal no válido")]
        public decimal Ti2 { get; set; }

        [RegularExpression(@"^[0-9]+([.][0-9]+)?$", ErrorMessage = "Número decimal no válido")]
        public decimal Te1 { get; set; }

        [RegularExpression(@"^[0-9]+([.][0-9]+)?$", ErrorMessage = "Número decimal no válido")]
        public decimal Te2 { get; set; }

        [RegularExpression(@"^[0-9]+([.][0-9]+)?$", ErrorMessage = "Número decimal no válido")]
        public decimal Tv1 { get; set; }

        [NotMapped]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Promedio {
            get
            {
                return ((Examen1 + Examen2 + Ti1 + Ti2 + Te1 + Te2 + Tv1)/7);
            }
        }
    }
}