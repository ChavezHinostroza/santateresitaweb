﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ColegioST.Models
{
    public class Alumno
    {
        public int AlumnoId { get; set; }

        [Required(ErrorMessage = "Debes ingresar un {0}")]
        [StringLength(30, ErrorMessage =
            "El campo {0} debe contener maximo {1} y minimo {2} caracteres",
            MinimumLength = 2)]
        [Display(Name = "Nombre")]
        [RegularExpression(@"^([a-zA-Z \.\&\'\-]+)$", ErrorMessage = "Nombre invalido")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Debes ingresar un {0}")]
        [StringLength(30, ErrorMessage =
            "El campo {0} debe contener maximo {1} y minimo {2} caracteres",
            MinimumLength = 2)]
        [Display(Name = "Apellido")]
        [RegularExpression(@"^([a-zA-Z \.\&\'\-]+)$", ErrorMessage = "Apellido invalido")]
        public string Apellido { get; set; }

        [RegularExpression(@"^[0-9]{8}$", ErrorMessage = "DNI invalido")]
        public string Dni { get; set; }
        public bool Estado { get; set; }

        //[RegularExpression(@"^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$", ErrorMessage = "Fecha de nacimiento invalida")]
        public DateTime FechaNac { get; set; }

        [Required(ErrorMessage = "Debes ingresar un {0}")]
        [StringLength(15, ErrorMessage =
            "El campo {0} debe contener maximo {1} y minimo {2} caracteres",
            MinimumLength = 7)]
        [Display(Name = "Nombre de User")]
        [RegularExpression(@"^([a-zA-Z0-9 \.\&\'\-]+)$", ErrorMessage = "Nombre de User invalido")]
        public string User { get; set; }

        [Required(ErrorMessage = "Debes ingresar un {0}")]
        [StringLength(15, ErrorMessage =
            "El campo {0} debe contener maximo {1} y minimo {2} caracteres",
            MinimumLength = 7)]
        [Display(Name = "Nombre de User")]
        [RegularExpression(@"^([a-zA-Z0-9 \.\&\'\-]+)$", ErrorMessage = "Password invalido")]
        public string Password { get; set; }

        //datos apoderado
        [Required(ErrorMessage = "Debes ingresar un {0}")]
        [StringLength(30, ErrorMessage =
            "El campo {0} debe contener maximo {1} y minimo {2} caracteres",
            MinimumLength = 2)]
        [Display(Name = "Nombre Apoderado")]
        [RegularExpression(@"^([a-zA-Z \.\&\'\-]+)$", ErrorMessage = "Nombre Apoderado invalido")]
        public string NombreApoderado { get; set; }

        [Required(ErrorMessage = "Debes ingresar un {0}")]
        [StringLength(30, ErrorMessage =
            "El campo {0} debe contener maximo {1} y minimo {2} caracteres",
            MinimumLength = 2)]
        [Display(Name = "Apellido Apoderado")]
        [RegularExpression(@"^([a-zA-Z \.\&\'\-]+)$", ErrorMessage = "Apellido Apoderado invalido")]
        public string ApellidoApoderado { get; set; }

        [Required(ErrorMessage = "Debes ingresar un {0}")]
        [StringLength(15, ErrorMessage =
            "El campo {0} debe contener maximo {1} y minimo {2} caracteres",
            MinimumLength = 7)]
        [Display(Name = "Nombre de User")]
        [RegularExpression(@"^([a-zA-Z0-9 \.\&\'\-]+)$", ErrorMessage = "Nombre de User invalido")]
        public string UserApoderado { get; set; }

        [Required(ErrorMessage = "Debes ingresar un {0}")]
        [StringLength(15, ErrorMessage =
            "El campo {0} debe contener maximo {1} y minimo {2} caracteres",
            MinimumLength = 7)]
        [Display(Name = "Nombre de User")]
        [RegularExpression(@"^([a-zA-Z0-9 \.\&\'\-]+)$", ErrorMessage = "Password invalido")]
        public string PasswordApoderado { get; set; }

        //muchos a muchos
        public virtual ICollection<AlumnoCurso> AlumnoCurso { get; set; }

    }
}