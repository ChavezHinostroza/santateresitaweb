﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ColegioST.Startup))]
namespace ColegioST
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
