﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ColegioST.Models;

namespace ColegioST.Controllers
{
    public class AlumnoCursoController : Controller
    {
        private DataContext db = new DataContext();

        // GET: AlumnoCurso
        public ActionResult Index(int DocenteId)
        {
            var alumnoCursoes = db.AlumnoCursoes.Include(a => a.Alumno).Include(a => a.Curso).Include(a => a.Curso.Docente);

            var docente = db.Docentes.Find(DocenteId); 

            ViewBag.NombreDocente = docente.Apellido + ", "+ docente.Nombre; 

            return View(alumnoCursoes.ToList().Where(d => d.Curso.DocenteId == DocenteId));
        }


        // GET: AlumnoVerNotas
        public ActionResult VerCursosAlumno(int AlumnoId, int Opcion)
        {
            var alumnoCursoes = db.AlumnoCursoes.Include(y => y.Alumno).Include(a => a.Curso);

            var lista = alumnoCursoes.ToList().Where(d => d.AlumnoId == AlumnoId);

            if(Opcion == 2)
            {
                ViewBag.NombreAlumno = "Bienvenido(a) " + lista.FirstOrDefault().Alumno.ApellidoApoderado + ", " + lista.FirstOrDefault().Alumno.NombreApoderado ;
            }else
            {
                ViewBag.NombreAlumno = "Bienvenido(a) " + lista.FirstOrDefault().Alumno.Apellido + ", " + lista.FirstOrDefault().Alumno.Nombre;
            }

            return View(lista);
        }


        // GET: AlumnoCurso/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlumnoCurso alumnoCurso = db.AlumnoCursoes.Find(id);
            if (alumnoCurso == null)
            {
                return HttpNotFound();
            }
            return View(alumnoCurso);
        }

        // GET: AlumnoCurso/Create
        public ActionResult Create()
        {
            ViewBag.AlumnoId = new SelectList(db.Alumnoes, "AlumnoId", "Nombre");
            ViewBag.CursoId = new SelectList(db.Cursoes, "CursoId", "Nombre");
            return View();
        }

        // POST: AlumnoCurso/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AlumnoId,CursoId,Examen1,Examen2,Ti1,Ti2,Te1,Te2,Tv1")] AlumnoCurso alumnoCurso)
        {
            if (ModelState.IsValid)
            {
                db.AlumnoCursoes.Add(alumnoCurso);
                db.SaveChanges();
                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                var doc = db.Cursoes.Find(alumnoCurso.CursoId);

                return RedirectToAction("Index", "AlumnoCurso", new { DocenteId = doc.DocenteId });

            }

            ViewBag.AlumnoId = new SelectList(db.Alumnoes, "AlumnoId", "Nombre", alumnoCurso.AlumnoId);
            ViewBag.CursoId = new SelectList(db.Cursoes, "CursoId", "Nombre", alumnoCurso.CursoId);
            return View(alumnoCurso);
        }

        // GET: AlumnoCurso/Edit/5
        public ActionResult Edit(int? id, int? AlumnoId, int? CursoId)
        {
            if (AlumnoId == null && CursoId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //AlumnoCurso alumnoCurso = db.AlumnoCursoes.Find(AlumnoId, CursoId);

            AlumnoCurso alumnoCurso = db.AlumnoCursoes
              .Include(i => i.Curso)
              .Include(i => i.Curso.Docente)
              .SingleOrDefault(x => x.AlumnoId == AlumnoId && x.CursoId == CursoId);

            if (alumnoCurso == null)
            {
                return HttpNotFound();
            }
            ViewBag.AlumnoId = new SelectList(db.Alumnoes, "AlumnoId", "Nombre", alumnoCurso.AlumnoId);
            ViewBag.CursoId = new SelectList(db.Cursoes, "CursoId", "Nombre", alumnoCurso.CursoId);

            return View(alumnoCurso);
        }

        // POST: AlumnoCurso/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AlumnoCurso alumnoCurso)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alumnoCurso).State = EntityState.Modified;
                db.SaveChanges();
                TempData["UpdateSuccess"] = "Se Actualizo Correctamente";

                var doc = db.Cursoes.Find(alumnoCurso.CursoId);
                
                return RedirectToAction("Index", "AlumnoCurso", new { DocenteId = doc.DocenteId });
            }
            ViewBag.AlumnoId = new SelectList(db.Alumnoes, "AlumnoId", "Nombre", alumnoCurso.AlumnoId);
            ViewBag.CursoId = new SelectList(db.Cursoes, "CursoId", "Nombre", alumnoCurso.CursoId);

            return View(alumnoCurso);
        }

        // GET: AlumnoCurso/Delete/5
        public ActionResult Delete(int? AlumnoId, int? CursoId)
        {
            AlumnoCurso alumnoCurso = db.AlumnoCursoes.Find(AlumnoId, CursoId);
            db.AlumnoCursoes.Remove(alumnoCurso);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //// POST: AlumnoCurso/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    AlumnoCurso alumnoCurso = db.AlumnoCursoes.Find(id);
        //    db.AlumnoCursoes.Remove(alumnoCurso);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
